# Simulateur d'Affichage d'Écran LCD

Ce projet est un simulateur d'affichage d'écran LCD en TypeScript. Il permet d'afficher des nombres sur un écran LCD
virtuel en utilisant une grille 3x3.

## Installation

Pour exécuter ce programme, assurez-vous d'avoir Node.js installé sur votre ordinateur. Si ce n'est pas le cas, vous
pouvez le télécharger depuis [le site officiel de Node.js](https://nodejs.org/).

Ensuite, installez les dépendances nécessaires en exécutant la commande suivante dans le répertoire du projet :

```bash
npm install
```

## Utilisation

Pour lancer le projet en mode de développement, exécutez la commande suivante :

```bash
npm run dev
```

Pour lancer le projet en mode production, exécutez la commande suivante :

```bash
npm run build
npm start
```

## Tests

Pour lancer les tests unitaires, exécutez la commande suivante :

```bash
npm run test
```

## Auteur
[Steven DONGMO](gitlab.com/mouafus)