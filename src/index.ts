import {createInterface} from 'readline';

const readline = createInterface({
    input: process.stdin,
    output: process.stdout
});


const lcdDigits: string[][] = [
    [' _ ', '| |', '|_|'],  // 0
    ['   ', '  |', '  |'],  // 1
    [' _ ', ' _|', '|_ '],  // 2
    [' _ ', ' _|', ' _|'],  // 3
    ['   ', '|_|', '  |'],  // 4
    [' _ ', '|_ ', ' _|'],  // 5
    [' _ ', '|_ ', '|_|'],  // 6
    [' _ ', '  |', '  |'],  // 7
    [' _ ', '|_|', '|_|'],  // 8
    [' _ ', '|_|', ' _|'],  // 9
];

/**
 * Affiche un chiffre sur l'écran LCD.
 * @param number - Le nombre à afficher.
 */
export function displayLCDNumber(number: string): void {
    const digits = number.split('');
    const lcdNumber = digits.map((digit) => lcdDigits[parseInt(digit, 10)]);
    const lcdNumberLines = [0, 1, 2].map((_, lineIndex) => {
        return lcdNumber.map((lcdDigit) => lcdDigit[lineIndex]).join(' ');
    })
    console.log(lcdNumberLines.join('\n'));
}

/**
 * Fonction pour demander à l'utilisateur s'il souhaite refaire l'exercice.
 */
function replayExercise(): void {
    readline.question('Voulez-vous refaire l\'exercice ? (Oui/Non) : ', (input) => {
        if (input.toLowerCase() === 'oui') {
            startExercise();
        } else {
            console.log('Au revoir !');
            console.clear();
            readline.close();
        }
    });
}

function startExercise(): void {
    readline.question('Veuillez entrer nombre : ', (input) => {
        if (isNaN(parseInt(input, 10))) {
            console.error('Le programme attend un nombre.');
            startExercise();
        } else {
            displayLCDNumber(input);
            replayExercise();
        }
    });
}

startExercise();
