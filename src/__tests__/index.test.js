const {displayLCDNumber} = require('../index');

test('Affiche le chiffre 0', () => {
    const consoleSpy = jest.spyOn(console, 'log').mockImplementation();
    displayLCDNumber('0');
    expect(consoleSpy).toHaveBeenCalledWith(' _ \n| |\n|_|');
    consoleSpy.mockRestore();
});
